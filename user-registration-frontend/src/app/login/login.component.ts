import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../service/user.service';
// import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User = new User();
  errorMessage: string;
  message: any;

  constructor(private service: UserService, private router: Router) { }



  ngOnInit() {
  }

  login() {
    let resp = this.service.login();
    resp.subscribe((data) => this.message = data);
    this.router.navigate(['/list-users']);
  }

}

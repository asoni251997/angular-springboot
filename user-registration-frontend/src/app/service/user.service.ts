import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }


  public doRegistration(user){
    return this.http.post("http://localhost:8080/process_register",user,{responseType:'text' as 'json'});
  }

 public login(){
  return this.http.get("http://localhost:8080/login");
 }

 public getUsers(){
  return this.http.get("http://localhost:8080/list_users");
}

public deleteUser(id){
  return this.http.delete("http://localhost:8080/delete/"+id);
}

public getUserByEmail(email){
  return this.http.get("http://localhost:8080/findUserByEmail/"+email);
}

public updateUser(id){
  return this.http.get("http://localhost:8080/edit/"+id);
}
  
}

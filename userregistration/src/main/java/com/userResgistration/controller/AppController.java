package com.userResgistration.controller;

import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.userResgistration.dao.UserRepository;
import com.userResgistration.entities.User;
import com.userResgistration.services.userService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AppController {
	
	public static final Logger logger = LoggerFactory.getLogger(AppController.class);
	
	@Autowired
	private UserRepository repo;
	
	@Autowired
	private userService service;

//	@GetMapping("")
//	public String viewHomepage() {
//		return "index";
//	}
	
//	@GetMapping("/login")
//	public Principal user(Principal principal) {
//		logger.info("user logged "+principal);
//		return principal;
//	}
	
	@GetMapping("/login")
	public ModelAndView login(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        logger.info("user logged successfully");
        return modelAndView;
	}
	
//	@GetMapping("/register")
//	public String signUpForm(Model model) {
//		
//		model.addAttribute("user", new User());
//		return "signUp_Form";
//	}
//	
	@PostMapping("/process_register")
	public String processValues(@RequestBody User user) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		
		repo.save(user);
		return "Registration process successfully completed";
	}
	
	@GetMapping("/list_users")
	 public List<User> findAllUsers() {
        return repo.findAll();
    }
	
	 @GetMapping("/findUserByEmail/{email}")
	    public List<User> getUser(@PathVariable String email) {
	        return repo.findByEmail(email);
	    }
	
//	@RequestMapping(value = "/save", method = RequestMethod.POST)
//	public String saveUset(@ModelAttribute("editUser") User user) {
//		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//		String encodedPassword = passwordEncoder.encode(user.getPassword());
//		user.setPassword(encodedPassword);
//		
//		service.save(user);
//		
//		return "Update_successForm";
//	}
//	
	@GetMapping("/edit/{id}")
	public ModelAndView updateUser(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("editUser");
		User user = service.get(id);
		mav.addObject("user", user);

		return mav;
	}
	
	@DeleteMapping("/delete/{id}")
	public List<User> deleteUser(@PathVariable(name = "id") int id) {
		service.delete(id);
		return repo.findAll();		
	}
	
}

package com.userResgistration.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.userResgistration.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {
	
//	@Query("SELECT u FROM User u WHERE u.email = ?1")
	List<User> findByEmail(String email);
	
//	List<User> findUsers(String firstname, String lastname, String address);
}
